## Flutter Environment

Version - [2.0.3] channel _stable_

### Running

**Development**
`flutter run -t lib/main_dev.dart --flavor dev`
**Production**
`flutter run -t lib/main_prod.dart --flavor prod`
