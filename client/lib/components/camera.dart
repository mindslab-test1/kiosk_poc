import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:client/model/image_model.dart';
import 'package:firebase_ml_vision/firebase_ml_vision.dart';
import 'package:flutter/material.dart';
import 'package:camera/camera.dart';
import 'package:http/http.dart';
import 'package:http_parser/http_parser.dart';
import 'package:image_picker/image_picker.dart';
import '../util/machine_learning.dart';
import 'package:path_provider/path_provider.dart';
import 'package:image/image.dart' as imglib;
import 'package:http/http.dart' as http;

class Camera extends StatefulWidget {
  @override
  _CameraState createState() => _CameraState();
}

String _baseUrl = "http://13.124.140.51:9000/kiosk/api/recogFace";
Future<Map<String, dynamic>> sendMultipartPostMethod(
    String url, Map<String, dynamic> body, List<MultipartFile> files) async {
  var sendUrl = Uri.parse(_baseUrl + url);
  try {
    var request = MultipartRequest('POST', sendUrl);
    if (body != null) {
      body.forEach((key, value) {
        request..fields[key] = value.toString();
      });
    }
    if (files != null) request.files?.addAll(files);

    print("FILES: ");
    print(request.files);

    StreamedResponse response = await request.send();
    print("response statuscode: " + response.statusCode.toString());
    response.stream.transform(utf8.decoder).listen((value) {
      print("VALUE");
      print(value);
    });
    return {
      "status": response.statusCode,
    };
  } catch (e) {
    return null;
  }
}

class _CameraState extends State<Camera> {
  File _file;
  File _pickedImage;
  CameraController controller;
  List<CameraDescription> cameras;
  MachineLearningLists machineLearningLists;

  final int _frontCamera = 1;
  final int _backCamera = 0;
  bool _isImageLoaded = false;
  bool _scanning = false;

  final picker = ImagePicker();

  @override
  void initState() {
    machineLearningLists = MachineLearning();
    super.initState();
    _setupCamera();
  }

  @override
  void dispose() {
    controller?.dispose();
    super.dispose();
  }

  void _goBackHome() {
    return Navigator.of(context).pop(true);
  }

  Future<void> _setupCamera() async {
    try {
      // initialize cameras.
      cameras = await availableCameras();

      //initialize camera controllers
      controller =
          CameraController(cameras[_frontCamera], ResolutionPreset.veryHigh);
      ;

      await controller.initialize().then((_) {
        if (!mounted) {
          return;
        }
        controller.startImageStream((CameraImage availableImage) {
          if (!_scanning) {
            _scanImage(availableImage);
            _scanning = true;
          }

          // controller.stopImageStream();
        });

        setState(() {});
      });
    } on CameraException catch (e) {
      print("ERROR " + e.code + ": " + e.description);
      //TODO
    }

    if (!mounted) {
      return;
    }

    setState(() {});
  }

  Future _pickImage() async {
    final _tempStore = await picker.getImage(source: ImageSource.gallery);
    setState(() {
      if (_tempStore != null) {
        _pickedImage = File(_tempStore.path);
        _isImageLoaded = true;
      } else {
        //TODO
      }
    });
  }

  Future _takeImage() async {
    final _tempStore = await picker.getImage(source: ImageSource.camera);
    setState(() {
      if (_tempStore != null) {
        _pickedImage = File(_tempStore.path);
        _isImageLoaded = true;
      } else {
        //TODO
      }
    });
  }

  Future _scanImage(CameraImage image) async {
    imglib.Image _imageFile = await convertYUV420toImage(image);
    Stopwatch stopwatch = Stopwatch()..start();
    imglib.Image originalImage = _imageFile;

    final FirebaseVisionImageMetadata metadata = FirebaseVisionImageMetadata(
        rawFormat: image.format.raw,
        size: Size(image.width.toDouble(), image.height.toDouble()),
        planeData: image.planes
            .map((currentPlane) => FirebaseVisionImagePlaneMetadata(
                bytesPerRow: currentPlane.bytesPerRow,
                height: currentPlane.height,
                width: currentPlane.width))
            .toList(),
        rotation: ImageRotation
            .rotation0); //! Need to Check Camera Rotation (Impacts Performance)

    if (image == null) {
      print("FILE NULL");
      return null;
    }
    FirebaseVisionImage visionImage =
        FirebaseVisionImage.fromBytes(image.planes[0].bytes, metadata);

    List<Map<String, int>> faceMaps =
        await machineLearningLists.scanFace(visionImage);

    if (faceMaps.isNotEmpty) {
      int _newX1 = faceMaps[0]['x'] - (faceMaps[0]['w'] * 0.25).toInt();
      int _newY1 = faceMaps[0]['y'] - (faceMaps[0]['h'] * 0.25).toInt();
      int _newW = (faceMaps[0]['w'] * 1.5).toInt();
      int _newH = (faceMaps[0]['h'] * 1.5).toInt();

      if (_newX1 > originalImage.width) {
        _newX1 = originalImage.width;
      }
      if (_newY1 < 0) {
        _newY1 = 0;
      }
      if ((_newW + _newX1) < 0) {
        _newW = 0;
      }
      if ((_newH + _newY1) > originalImage.height) {
        _newH = originalImage.height;
      }

      imglib.Image faceCrop =
          imglib.copyCrop(originalImage, _newX1, _newY1, _newW, _newH);

      // print("RGB: ");
      // print(faceCrop.data);

      Directory tempDir = await getApplicationDocumentsDirectory();
      String tempPath = tempDir.path;

      _file = await File(tempPath + '/cropped_image.png')
          .writeAsBytes(imglib.encodePng(faceCrop));
    }

    //! ADD SENDING REQUEST HERE

    //* SENDING IMAGE REST API CALL HERE
    List<MultipartFile> _fileImage = <MultipartFile>[];
    _fileImage.add(
      http.MultipartFile(
        'image',
        _file.readAsBytes().asStream(),
        _file.lengthSync(),
        filename: "face",
        contentType: MediaType('image', 'png'),
      ),
    );
    Map<String, dynamic> _body = {'dbId': "default"};

    Map<String, dynamic> _responseMap =
        await sendMultipartPostMethod("", _body, _fileImage);
    //! ERASE EVERYTHING ABOVE

    print('ElAPESD: ${stopwatch.elapsed}');
    setState(() {
      _scanning = false;
    });
  }

  Future<imglib.Image> convertYUV420toImage(CameraImage image) async {
    try {
      final int width = image.width;
      final int height = image.height;

      var img = imglib.Image(width, height);

      for (int x = 0; x < width; x++) {
        for (int y = 0; y < height; y++) {
          final pixelColor = image.planes[0].bytes[y * width + x];
          img.data[y * width + x] = (0xFF << 24) |
              (pixelColor << 16) |
              (pixelColor << 8) |
              pixelColor;
        }
      }
      return img;
    } catch (e) {
      print("ERROR while converting image: " + e.toString());
      //TODO throw Error
    }
    return null;
  }

  @override
  Widget build(BuildContext context) {
    if (controller == null || !controller.value.isInitialized) {
      return Container();
    }
    return Stack(
      fit: StackFit.expand,
      children: <Widget>[
        CameraPreview(controller),
        //* FOR DEVELOPMENT OPTION OF VIEWING FD IN CAMERA SCREEN
        // Center(
        //   child: _file == null
        //       ? Container()
        //       : Container(
        //           height: 100,
        //           width: 100,
        //           decoration: BoxDecoration(
        //             image: DecorationImage(
        //               image: FileImage(_file),
        //               fit: BoxFit.cover,
        //             ),
        //           ),
        //         ),
        // ),
      ],
    );
  }
}
