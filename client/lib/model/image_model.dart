// To parse this JSON data, do
//
//     final imageModel = imageModelFromJson(jsonString);

import 'dart:convert';

ImageModel imageModelFromJson(String str) =>
    ImageModel.fromJson(json.decode(str));

String imageModelToJson(ImageModel data) => json.encode(data.toJson());

class ImageModel {
  ImageModel({
    this.success,
    this.statusCode,
    this.statusDetail,
    this.timestamp,
    this.detail,
  });

  bool success;
  int statusCode;
  String statusDetail;
  int timestamp;
  String detail;

  factory ImageModel.fromJson(Map<String, dynamic> json) => ImageModel(
        success: json["success"],
        statusCode: json["status_code"],
        statusDetail: json["status_detail"],
        timestamp: json["timestamp"],
        detail: json["detail"],
      );

  Map<String, dynamic> toJson() => {
        "success": success,
        "status_code": statusCode,
        "status_detail": statusDetail,
        "timestamp": timestamp,
        "detail": detail,
      };
}
