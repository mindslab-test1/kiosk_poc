import 'package:firebase_ml_vision/firebase_ml_vision.dart';
import 'package:flutter/material.dart';

abstract class MachineLearningLists {
  Future<List<Map<String, int>>> scanFace(FirebaseVisionImage file);
  void disposeLabeler();
}

class MachineLearning implements MachineLearningLists {
  FirebaseVisionImage visionImage;
  final FaceDetector faceDetector = FirebaseVision.instance
      .faceDetector(FaceDetectorOptions(mode: FaceDetectorMode.fast));

  @override
  void disposeLabeler() {
    faceDetector.close();
  }

  @override
  Future<List<Map<String, int>>> scanFace(
      FirebaseVisionImage visionImage) async {
    final List<Face> faces = await faceDetector.processImage(visionImage);
    List<Map<String, int>> faceMaps = [];
    for (Face face in faces) {
      int x = face.boundingBox.left.toInt();
      int y = face.boundingBox.top.toInt();
      int w = face.boundingBox.width.toInt();
      int h = face.boundingBox.height.toInt();
      Map<String, int> thisMap = {'x': x, 'y': y, 'w': w, 'h': h};
      faceMaps.add(thisMap);

      final Rect boundingBox = face.boundingBox;

      //* Will Later Need for Multiple Face Tracking
      // If face tracking was enabled with FaceDetectorOptions:
      if (face.trackingId != null) {
        final int id = face.trackingId;
      }
      print("BOUNDING:");
      print(boundingBox);
    }
    return faceMaps;
  }
}
