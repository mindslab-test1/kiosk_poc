package ai.mindslab.kioskback.api.common.controller;

import ai.mindslab.kioskback.api.common.service.LogService;
import ai.mindslab.kioskback.api.common.vo.LogRequestVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

@Slf4j
@Controller
@RequestMapping("/log")
public class LogController {
    @Autowired
    @Qualifier("sysLogServiceImpl")
    LogService sysLogService;

    @Autowired
    @Qualifier("userLogServiceImpl")
    LogService userLogService;

    @PostMapping("/sys")
    public void generateSysLogs(@RequestBody LogRequestVo logRequestVo) {
        if (logRequestVo.getStatus().toUpperCase().equals("ERROR")) {
            sysLogService.errorWithImage(logRequestVo.getImgData(), logRequestVo.getVersion(), logRequestVo.getSource(), logRequestVo.getMssg(), logRequestVo.getTimestamp());
        } else if (logRequestVo.getStatus().toUpperCase().equals("WARNING")) {
            sysLogService.warnWithImage(logRequestVo.getImgData(), logRequestVo.getVersion(), logRequestVo.getSource(), logRequestVo.getMssg(), logRequestVo.getTimestamp());
        } else if (logRequestVo.getStatus().toUpperCase().equals("INFO")) {
            sysLogService.infoWithImage(logRequestVo.getImgData(), logRequestVo.getVersion(), logRequestVo.getSource(), logRequestVo.getMssg(), logRequestVo.getTimestamp());
        }
    }
}
