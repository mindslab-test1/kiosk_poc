package ai.mindslab.kioskback.api.common.domain;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;

@DynamicInsert
@DynamicUpdate
@Getter
@Setter
@Entity
@Table(name = "LOG_IMAGE")
public class LogImage {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "NO")
    private Long no;

    @Column(name = "IMAGE_DATA", columnDefinition = "blob")
    private byte[] imgData;

}
