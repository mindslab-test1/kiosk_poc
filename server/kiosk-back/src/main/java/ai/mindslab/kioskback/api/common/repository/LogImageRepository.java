package ai.mindslab.kioskback.api.common.repository;

import ai.mindslab.kioskback.api.common.domain.LogImage;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LogImageRepository extends JpaRepository<LogImage, Long> {

}
