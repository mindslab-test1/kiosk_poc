package ai.mindslab.kioskback.api.common.service;

public interface LogService {
    public void infoWithImage(byte[] imgData, String version, String source, String mssg, String timestamp);

    public void warnWithImage(byte[] imgData, String version, String source, String mssg, String timestamp);

    public void errorWithImage(byte[] imgData, String version, String source, String mssg, String timestamp);

    public void info(String format, Object... arguments);

    public void warn(String format, Object... arguments);

    public void error(String format, Object... arguments);
}
