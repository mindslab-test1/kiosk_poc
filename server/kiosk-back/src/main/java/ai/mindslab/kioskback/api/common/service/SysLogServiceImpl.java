package ai.mindslab.kioskback.api.common.service;

import ai.mindslab.kioskback.api.common.domain.LogImage;
import ai.mindslab.kioskback.api.common.repository.LogImageRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class SysLogServiceImpl implements LogService {
    private static final Logger sys_logger = LoggerFactory.getLogger("system");

    @Value("${kiosk.url}")
    private String url;

    @Autowired
    private LogImageRepository logImageRepository;



    @Override
    public void infoWithImage(byte[] imgData, String version, String source, String mssg, String timestamp) {
        String imageUrl = "";

        if (imgData.length != 0) {
            LogImage logImage = new LogImage();
            logImage.setImgData(imgData);

            LogImage result = logImageRepository.save(logImage);
            imageUrl = url + "/sys/img/" + result.getNo().toString();
        } else {
            sys_logger.info("(Failed to save image)({})({})({})({})", version, source, mssg, timestamp);

        }

        sys_logger.info("({})({})({})({})({})", imageUrl, version, source, mssg, timestamp);

    }

    @Override
    public void warnWithImage(byte[] imgData, String version, String source, String mssg, String timestamp) {
        String imageUrl = "";

        if (imgData.length != 0) {
            LogImage logImage = new LogImage();
            logImage.setImgData(imgData);

            LogImage result = logImageRepository.save(logImage);
            imageUrl = url +"/sys/img/" + result.getNo().toString();
        } else {
            sys_logger.warn("(Failed to save image)({})({})({})({})", version, source, mssg, timestamp);

        }

        sys_logger.warn("({})({})({})({})({})", imageUrl, version, source, mssg, timestamp);
    }

    @Override
    public void errorWithImage(byte[] imgData, String version, String source, String mssg, String timestamp) {
        String imageUrl = "";

        if (imgData.length != 0) {
            LogImage logImage = new LogImage();
            logImage.setImgData(imgData);

            LogImage result = logImageRepository.save(logImage);
            imageUrl = url +"/sys/img/" + result.getNo().toString();
        } else {
            sys_logger.error("(Failed to save image)({})({})({})({})", version, source, mssg, timestamp);

        }

        sys_logger.error("({})({})({})({})({})", imageUrl, version, source, mssg, timestamp);
    }

    @Override
    public void info(String format, Object... arguments) {
        sys_logger.info(format, arguments);
    }

    @Override
    public void warn(String format, Object... arguments) {
        sys_logger.warn(format, arguments);
    }

    @Override
    public void error(String format, Object... arguments) {
        sys_logger.error(format, arguments);
    }
}
