package ai.mindslab.kioskback.api.common.service;

import ai.mindslab.kioskback.api.common.domain.LogImage;
import ai.mindslab.kioskback.api.common.repository.LogImageRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class UserLogServiceImpl implements LogService{
    private static final Logger user_logger = LoggerFactory.getLogger("user");

    @Value("${kiosk.url}")
    private String url;

    @Autowired
    private LogImageRepository logImageRepository;

    @Override
    public void infoWithImage(byte[] imgData, String version, String source, String mssg, String timestamp) {
        String imageUrl = "";

        if (imgData.length != 0) {
            LogImage logImage = new LogImage();
            logImage.setImgData(imgData);

            LogImage result = logImageRepository.save(logImage);
            imageUrl = url +"/user/img/" + result.getNo().toString();
        } else {
            user_logger.info("(Failed to save image)({})({})({})({})", version, source, mssg, timestamp);

        }

        user_logger.info("({})({})({})({})({})", imageUrl, version, source, mssg, timestamp);

    }

    @Override
    public void warnWithImage(byte[] imgData, String version, String source, String mssg, String timestamp) {
        String imageUrl = "";

        if (imgData.length != 0) {
            LogImage logImage = new LogImage();
            logImage.setImgData(imgData);

            LogImage result = logImageRepository.save(logImage);
            imageUrl = url +"/user/img/" + result.getNo().toString();
        } else {
            user_logger.warn("(Failed to save image)({})({})({})({})", version, source, mssg, timestamp);

        }

        user_logger.warn("({})({})({})({})({})", imageUrl, version, source, mssg, timestamp);
    }

    @Override
    public void errorWithImage(byte[] imgData, String version, String source, String mssg, String timestamp) {
        String imageUrl = "";

        if (imgData.length != 0) {
            LogImage logImage = new LogImage();
            logImage.setImgData(imgData);

            LogImage result = logImageRepository.save(logImage);
            imageUrl = url +"/user/img/" + result.getNo().toString();
        } else {
            user_logger.error("(Failed to save image)({})({})({})({})", version, source, mssg, timestamp);
        }

        user_logger.error("({})({})({})({})({})", imageUrl, version, source, mssg, timestamp);
    }

    @Override
    public void info(String format, Object... arguments) {
        user_logger.info(format, arguments);
    }

    @Override
    public void warn(String format, Object... arguments) {
        user_logger.warn(format, arguments);
    }

    @Override
    public void error(String format, Object... arguments) {
        user_logger.error(format, arguments);
    }
}
