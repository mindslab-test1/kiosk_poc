package ai.mindslab.kioskback.api.common.vo;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class LogRequestVo {
    private String version;
    private String source;
    private String status;
    private String mssg;
    private byte[] imgData;
    private String timestamp;
}
