package ai.mindslab.kioskback.configurers;

import com.zaxxer.hikari.HikariDataSource;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import javax.sql.DataSource;

@Configuration
@EnableJpaRepositories(
    basePackages = {"ai.mindslab.kioskback.api"}
)
public class DataSourceConfiguration {
    @Bean(name = "kioskBackDataSource")
    @Primary
    @ConfigurationProperties("spring.datasource.hikari")
    public DataSource mariaDataSource() {
        return DataSourceBuilder.create().type(HikariDataSource.class).build();
    }
}
