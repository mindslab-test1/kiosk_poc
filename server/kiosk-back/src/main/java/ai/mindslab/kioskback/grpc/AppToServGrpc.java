package ai.mindslab.kioskback.grpc;

import static io.grpc.MethodDescriptor.generateFullMethodName;
import static io.grpc.stub.ClientCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ClientCalls.asyncClientStreamingCall;
import static io.grpc.stub.ClientCalls.asyncServerStreamingCall;
import static io.grpc.stub.ClientCalls.asyncUnaryCall;
import static io.grpc.stub.ClientCalls.blockingServerStreamingCall;
import static io.grpc.stub.ClientCalls.blockingUnaryCall;
import static io.grpc.stub.ClientCalls.futureUnaryCall;
import static io.grpc.stub.ServerCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ServerCalls.asyncClientStreamingCall;
import static io.grpc.stub.ServerCalls.asyncServerStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnaryCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall;

/**
 */
@javax.annotation.Generated(
    value = "by gRPC proto compiler (version 1.15.0)",
    comments = "Source: AppToServ.proto")
public final class AppToServGrpc {

  private AppToServGrpc() {}

  public static final String SERVICE_NAME = "AppToServ";

  // Static method descriptors that strictly reflect the proto.
  private static volatile io.grpc.MethodDescriptor<ai.mindslab.kioskback.grpc.AppToServOuterClass.FDRequest,
      ai.mindslab.kioskback.grpc.AppToServOuterClass.FDResponse> getFdResultMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "fdResult",
      requestType = ai.mindslab.kioskback.grpc.AppToServOuterClass.FDRequest.class,
      responseType = ai.mindslab.kioskback.grpc.AppToServOuterClass.FDResponse.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<ai.mindslab.kioskback.grpc.AppToServOuterClass.FDRequest,
      ai.mindslab.kioskback.grpc.AppToServOuterClass.FDResponse> getFdResultMethod() {
    io.grpc.MethodDescriptor<ai.mindslab.kioskback.grpc.AppToServOuterClass.FDRequest, ai.mindslab.kioskback.grpc.AppToServOuterClass.FDResponse> getFdResultMethod;
    if ((getFdResultMethod = AppToServGrpc.getFdResultMethod) == null) {
      synchronized (AppToServGrpc.class) {
        if ((getFdResultMethod = AppToServGrpc.getFdResultMethod) == null) {
          AppToServGrpc.getFdResultMethod = getFdResultMethod = 
              io.grpc.MethodDescriptor.<ai.mindslab.kioskback.grpc.AppToServOuterClass.FDRequest, ai.mindslab.kioskback.grpc.AppToServOuterClass.FDResponse>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "AppToServ", "fdResult"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  ai.mindslab.kioskback.grpc.AppToServOuterClass.FDRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  ai.mindslab.kioskback.grpc.AppToServOuterClass.FDResponse.getDefaultInstance()))
                  .setSchemaDescriptor(new AppToServMethodDescriptorSupplier("fdResult"))
                  .build();
          }
        }
     }
     return getFdResultMethod;
  }

  private static volatile io.grpc.MethodDescriptor<ai.mindslab.kioskback.grpc.AppToServOuterClass.SysRequest,
      ai.mindslab.kioskback.grpc.AppToServOuterClass.SysResponse> getSysLogMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "sysLog",
      requestType = ai.mindslab.kioskback.grpc.AppToServOuterClass.SysRequest.class,
      responseType = ai.mindslab.kioskback.grpc.AppToServOuterClass.SysResponse.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<ai.mindslab.kioskback.grpc.AppToServOuterClass.SysRequest,
      ai.mindslab.kioskback.grpc.AppToServOuterClass.SysResponse> getSysLogMethod() {
    io.grpc.MethodDescriptor<ai.mindslab.kioskback.grpc.AppToServOuterClass.SysRequest, ai.mindslab.kioskback.grpc.AppToServOuterClass.SysResponse> getSysLogMethod;
    if ((getSysLogMethod = AppToServGrpc.getSysLogMethod) == null) {
      synchronized (AppToServGrpc.class) {
        if ((getSysLogMethod = AppToServGrpc.getSysLogMethod) == null) {
          AppToServGrpc.getSysLogMethod = getSysLogMethod = 
              io.grpc.MethodDescriptor.<ai.mindslab.kioskback.grpc.AppToServOuterClass.SysRequest, ai.mindslab.kioskback.grpc.AppToServOuterClass.SysResponse>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "AppToServ", "sysLog"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  ai.mindslab.kioskback.grpc.AppToServOuterClass.SysRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  ai.mindslab.kioskback.grpc.AppToServOuterClass.SysResponse.getDefaultInstance()))
                  .setSchemaDescriptor(new AppToServMethodDescriptorSupplier("sysLog"))
                  .build();
          }
        }
     }
     return getSysLogMethod;
  }

  /**
   * Creates a new async stub that supports all call types for the service
   */
  public static AppToServStub newStub(io.grpc.Channel channel) {
    return new AppToServStub(channel);
  }

  /**
   * Creates a new blocking-style stub that supports unary and streaming output calls on the service
   */
  public static AppToServBlockingStub newBlockingStub(
      io.grpc.Channel channel) {
    return new AppToServBlockingStub(channel);
  }

  /**
   * Creates a new ListenableFuture-style stub that supports unary calls on the service
   */
  public static AppToServFutureStub newFutureStub(
      io.grpc.Channel channel) {
    return new AppToServFutureStub(channel);
  }

  /**
   */
  public static abstract class AppToServImplBase implements io.grpc.BindableService {

    /**
     */
    public void fdResult(ai.mindslab.kioskback.grpc.AppToServOuterClass.FDRequest request,
        io.grpc.stub.StreamObserver<ai.mindslab.kioskback.grpc.AppToServOuterClass.FDResponse> responseObserver) {
      asyncUnimplementedUnaryCall(getFdResultMethod(), responseObserver);
    }

    /**
     */
    public void sysLog(ai.mindslab.kioskback.grpc.AppToServOuterClass.SysRequest request,
        io.grpc.stub.StreamObserver<ai.mindslab.kioskback.grpc.AppToServOuterClass.SysResponse> responseObserver) {
      asyncUnimplementedUnaryCall(getSysLogMethod(), responseObserver);
    }

    @java.lang.Override public final io.grpc.ServerServiceDefinition bindService() {
      return io.grpc.ServerServiceDefinition.builder(getServiceDescriptor())
          .addMethod(
            getFdResultMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                ai.mindslab.kioskback.grpc.AppToServOuterClass.FDRequest,
                ai.mindslab.kioskback.grpc.AppToServOuterClass.FDResponse>(
                  this, METHODID_FD_RESULT)))
          .addMethod(
            getSysLogMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                ai.mindslab.kioskback.grpc.AppToServOuterClass.SysRequest,
                ai.mindslab.kioskback.grpc.AppToServOuterClass.SysResponse>(
                  this, METHODID_SYS_LOG)))
          .build();
    }
  }

  /**
   */
  public static final class AppToServStub extends io.grpc.stub.AbstractStub<AppToServStub> {
    private AppToServStub(io.grpc.Channel channel) {
      super(channel);
    }

    private AppToServStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected AppToServStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new AppToServStub(channel, callOptions);
    }

    /**
     */
    public void fdResult(ai.mindslab.kioskback.grpc.AppToServOuterClass.FDRequest request,
        io.grpc.stub.StreamObserver<ai.mindslab.kioskback.grpc.AppToServOuterClass.FDResponse> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getFdResultMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void sysLog(ai.mindslab.kioskback.grpc.AppToServOuterClass.SysRequest request,
        io.grpc.stub.StreamObserver<ai.mindslab.kioskback.grpc.AppToServOuterClass.SysResponse> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getSysLogMethod(), getCallOptions()), request, responseObserver);
    }
  }

  /**
   */
  public static final class AppToServBlockingStub extends io.grpc.stub.AbstractStub<AppToServBlockingStub> {
    private AppToServBlockingStub(io.grpc.Channel channel) {
      super(channel);
    }

    private AppToServBlockingStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected AppToServBlockingStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new AppToServBlockingStub(channel, callOptions);
    }

    /**
     */
    public ai.mindslab.kioskback.grpc.AppToServOuterClass.FDResponse fdResult(ai.mindslab.kioskback.grpc.AppToServOuterClass.FDRequest request) {
      return blockingUnaryCall(
          getChannel(), getFdResultMethod(), getCallOptions(), request);
    }

    /**
     */
    public ai.mindslab.kioskback.grpc.AppToServOuterClass.SysResponse sysLog(ai.mindslab.kioskback.grpc.AppToServOuterClass.SysRequest request) {
      return blockingUnaryCall(
          getChannel(), getSysLogMethod(), getCallOptions(), request);
    }
  }

  /**
   */
  public static final class AppToServFutureStub extends io.grpc.stub.AbstractStub<AppToServFutureStub> {
    private AppToServFutureStub(io.grpc.Channel channel) {
      super(channel);
    }

    private AppToServFutureStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected AppToServFutureStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new AppToServFutureStub(channel, callOptions);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<ai.mindslab.kioskback.grpc.AppToServOuterClass.FDResponse> fdResult(
        ai.mindslab.kioskback.grpc.AppToServOuterClass.FDRequest request) {
      return futureUnaryCall(
          getChannel().newCall(getFdResultMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<ai.mindslab.kioskback.grpc.AppToServOuterClass.SysResponse> sysLog(
        ai.mindslab.kioskback.grpc.AppToServOuterClass.SysRequest request) {
      return futureUnaryCall(
          getChannel().newCall(getSysLogMethod(), getCallOptions()), request);
    }
  }

  private static final int METHODID_FD_RESULT = 0;
  private static final int METHODID_SYS_LOG = 1;

  private static final class MethodHandlers<Req, Resp> implements
      io.grpc.stub.ServerCalls.UnaryMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ServerStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ClientStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.BidiStreamingMethod<Req, Resp> {
    private final AppToServImplBase serviceImpl;
    private final int methodId;

    MethodHandlers(AppToServImplBase serviceImpl, int methodId) {
      this.serviceImpl = serviceImpl;
      this.methodId = methodId;
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public void invoke(Req request, io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        case METHODID_FD_RESULT:
          serviceImpl.fdResult((ai.mindslab.kioskback.grpc.AppToServOuterClass.FDRequest) request,
              (io.grpc.stub.StreamObserver<ai.mindslab.kioskback.grpc.AppToServOuterClass.FDResponse>) responseObserver);
          break;
        case METHODID_SYS_LOG:
          serviceImpl.sysLog((ai.mindslab.kioskback.grpc.AppToServOuterClass.SysRequest) request,
              (io.grpc.stub.StreamObserver<ai.mindslab.kioskback.grpc.AppToServOuterClass.SysResponse>) responseObserver);
          break;
        default:
          throw new AssertionError();
      }
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public io.grpc.stub.StreamObserver<Req> invoke(
        io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        default:
          throw new AssertionError();
      }
    }
  }

  private static abstract class AppToServBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoFileDescriptorSupplier, io.grpc.protobuf.ProtoServiceDescriptorSupplier {
    AppToServBaseDescriptorSupplier() {}

    @java.lang.Override
    public com.google.protobuf.Descriptors.FileDescriptor getFileDescriptor() {
      return ai.mindslab.kioskback.grpc.AppToServOuterClass.getDescriptor();
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.ServiceDescriptor getServiceDescriptor() {
      return getFileDescriptor().findServiceByName("AppToServ");
    }
  }

  private static final class AppToServFileDescriptorSupplier
      extends AppToServBaseDescriptorSupplier {
    AppToServFileDescriptorSupplier() {}
  }

  private static final class AppToServMethodDescriptorSupplier
      extends AppToServBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoMethodDescriptorSupplier {
    private final String methodName;

    AppToServMethodDescriptorSupplier(String methodName) {
      this.methodName = methodName;
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.MethodDescriptor getMethodDescriptor() {
      return getServiceDescriptor().findMethodByName(methodName);
    }
  }

  private static volatile io.grpc.ServiceDescriptor serviceDescriptor;

  public static io.grpc.ServiceDescriptor getServiceDescriptor() {
    io.grpc.ServiceDescriptor result = serviceDescriptor;
    if (result == null) {
      synchronized (AppToServGrpc.class) {
        result = serviceDescriptor;
        if (result == null) {
          serviceDescriptor = result = io.grpc.ServiceDescriptor.newBuilder(SERVICE_NAME)
              .setSchemaDescriptor(new AppToServFileDescriptorSupplier())
              .addMethod(getFdResultMethod())
              .addMethod(getSysLogMethod())
              .build();
        }
      }
    }
    return result;
  }
}
